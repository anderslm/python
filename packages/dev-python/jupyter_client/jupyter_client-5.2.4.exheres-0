# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

# Python 3 only because ipython is the only dependent.
require pypi setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="Jupyter protocol implementation and client libraries"
HOMEPAGE+=" https://jupyter.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/jupyter_core[python_abis:*(-)?]
        dev-python/python-dateutil[>=2.1][python_abis:*(-)?]
        dev-python/pyzmq[>=13][python_abis:*(-)?]
        dev-python/tornado[>=4.1][python_abis:*(-)?]
        dev-python/traitlets[python_abis:*(-)?]
    test:
        dev-python/mock[python_abis:*(-)?]
    suggestion:
        (
            dev-python/ipykernel[python_abis:*(-)?]
            dev-python/ipython[python_abis:*(-)?]
        ) [[ *description = [ Moved test dependency to break cycle ] ]]
"

_are_test_deps_installed() {
    has_version "dev-python/ipython[python_abis:$(python_get_abi)]" || return 1
    has_version "dev-python/ipykernel[python_abis:$(python_get_abi)]" || return 1
}

test_one_multibuild() {
    if _are_test_deps_installed; then
        esandbox allow_net "unix:test-*"
        setup-py_test_one_multibuild
        esandbox disallow_net "unix:test-*"
    else
        ewarn "Test dependencies are not installed yet, skip tests"
    fi
}

