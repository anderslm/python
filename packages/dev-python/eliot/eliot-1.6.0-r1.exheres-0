# Copyright 2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=2 has_bin=true test=pytest ]

SUMMARY="Logging library that tells you why it happened"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/boltons[python_abis:*(-)?]
        dev-python/pyrsistent[>=0.11.8][python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/zopeinterface[python_abis:*(-)?]
    test:
        dev-python/hypothesis[python_abis:*(-)?]
        dev-python/testtools[python_abis:*(-)?]
    suggestion:
        dev-python/cffi[>=1.1.2][python_abis:*(-)?] [[
            description = [ systemd-journald support ]
        ]]
"

# disable failing tests, last checked: 1.6.0
PYTEST_PARAMS=(
    -k "not test_not_json_message and not test_missing_required_field and not test_help and not test_default_encoder_is_EliotJSONEncoder"
)

prepare_one_multibuild() {
    default

    # python3 UnicodeDecodeError, last checked: 1.6.0
    edo rm README.rst
    edo touch README.rst
}

