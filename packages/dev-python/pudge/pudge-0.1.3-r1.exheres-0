# Copyright 2008 Ali Polatel
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pudge-0.1.3.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi
require setup-py [ import=setuptools blacklist=3 ]

SUMMARY="A documentation generator for Python projects, using Restructured Text"
HOMEPAGE="http://pudge.lesscode.org"
LICENCES="MIT"
DESCRIPTION="
Pudge is a documentation system for Python projects.
Its features include:
* Generate documentation for Python packages, modules, classes, functions,
  and methods.
* Module and Class index hierarchies.
* Support for Restructured Text in docstrings.
* Easily apply common free documentation licenses (GNU, CC).
* Syntax coloured source HTML generation with anchors for
  line numbers.
* Generated reference documents link to source for all modules, classes,
  functions, and methods.
* Basic Restructured Text document templating (brings external documents into
  the flow of generated pages).
* Support for HTML 4.01 or XHTML 1.0 output.
* Basic Trac integration (adds Trac project links to navigational elements).
* Uses a combination of runtime inspection and Python source code scanning.
* Extensible and customizable using kid templates.
"

BUGS_TO="alip@exherbo.org"
REMOTE_IDS="pypi:pudge"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? (
            dev-python/docutils[python_abis:*(-)?]
            dev-python/kid[>=0.9.5][python_abis:*(-)?]
        )
    run:
        dev-python/docutils[python_abis:*(-)?]
        dev-python/kid[>=0.9.5][python_abis:*(-)?]
"

install_one_multibuild() {
    setup-py_install_one_multibuild
    if option doc; then
        einfo "Generating docs as requested..."
        ./bin/pudge --modules=pudge \
            --documents=doc/index.rst \
            --dest=doc/html || die "pudge failed"

        insinto /usr/share/doc/${PNVR}/html
        doins -r doc/html/*
    fi
}

