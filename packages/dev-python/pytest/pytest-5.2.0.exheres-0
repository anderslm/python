# Copyright 2013 Jakob Nixdorf <flocke@shadowice.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist=2 import=setuptools has_bin=true python_opts='[sqlite]' ]

SUMMARY="Powerful and scalable testing library for python development"
DESCRIPTION="
The py.test library provides full-featured and mature testing environment
that can scale from simple unit testing to complex functional testing.
It includes many common testing methods as well as an extensive plugin and
customization system.
"

BUGS_TO="flocke@shadowice.org"

UPSTREAM_CHANGELOG="https://pytest.org/latest/changelog.html"
UPSTREAM_DOCUMENTATION="https://pytest.org/latest/contents.html"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

# Also listed in [testing] extras:
# - argcomplete (makes sense to add only when completion is going be installed)
# - requests (can't see where it's used)
# - xmlschema (unpackaged)
DEPENDENCIES="
    build:
        dev-python/setuptools_scm[python_abis:*(-)?]
    build+run:
        dev-python/atomicwrites[>=1.0][python_abis:*(-)?]
        dev-python/attrs[>=17.4.0][python_abis:*(-)?]
        dev-python/more-itertools[>=4.0.0][python_abis:*(-)?]
        dev-python/packaging[python_abis:*(-)?]
        dev-python/pluggy[>=0.12&<1.0][python_abis:*(-)?]
        dev-python/py[>=1.5.0][python_abis:*(-)?]
        dev-python/wcwidth[python_abis:*(-)?]
        python_abis:3.5? ( dev-python/pathlib2[>=2.2.0][python_abis:3.5] )
        python_abis:3.5? ( dev-python/importlib_metadata[>=0.12][python_abis:3.5] )
        python_abis:3.6? ( dev-python/importlib_metadata[>=0.12][python_abis:3.6] )
        python_abis:3.7? ( dev-python/importlib_metadata[>=0.12][python_abis:3.7] )
    test:
        dev-python/hypothesis[>=3.56][python_abis:*(-)?]
    suggestion:
        dev-python/pytest-twisted[python_abis:*(-)?] [[
            description = [ write tests for twisted apps ]
        ]]
        dev-python/mock[python_abis:*(-)?] [[
            description = [ mocking functionality ]
        ]]
        dev-python/nose[python_abis:*(-)?] [[
            description = [ run test suites written for nose ]
        ]]
        dev-python/pexpect[python_abis:*(-)?] [[
            description = [ support for testing py.test and py.test plugins ]
        ]]
        dev-python/pytest-xdist[python_abis:*(-)?] [[
            description = [ to distribute tests to CPUs and remote hosts ]
        ]]
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild
    edo rm testing/test_junitxml.py # xmlschema unpackaged
}

test_one_multibuild() {
    # Last checked: pytest-5.2.0 on python 3.7.5 and 3.8_rc1.
    local disabled_tests=(
        "not TestTrialUnittest" # Fails when Twisted is installed.
        "and not cached_pyc_includes_pytest_version" # tries to write bytecode in /
        "and not pyc_vs_pyo" # Tries to write bytecode in /.
        "and not test_early_load_setuptools_name" # plugins?
        "and not test_installed_plugin_rewrite" # Loading plugins disabled.
        "and not test_pdb_custom_cls_with_set_trace" # pexpect can"t work under paludis?
        "and not test_plugin_preparse_prevents_setuptools_loading" # Plugins obviously.
        "and not test_preparse_ordering_with_setuptools" # Loading plugins disabled.
        "and not test_setuptools_importerror_issue1479" # Probably disabled plugins again.
        "and not TestAssert_reprcompare_attrsclass" # attrs issue?
        "and not xdist" # xdist plugin disabled.
    )
    # Only load chosen plugins for reproducible testing.
    PYTHONPATH=$(echo $(pwd)/build/lib*) \
        PYTEST_DISABLE_PLUGIN_AUTOLOAD=1 \
        PYTEST_PLUGINS="hypothesis" \
        edo ${PYTHON} -B src/pytest.py testing \
            -k "${disabled_tests[*]}" --maxfail=5
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    edo cp -a "${IMAGE}"/usr/$(exhost --target)/bin/py.test{,-$(python_get_abi)}
}

