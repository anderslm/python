# Copyright 2009, 2010, 2011, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=zdohnal release=${PV} suffix=tar.xz ] \
    python [ blacklist='2' multibuild=false ] \
    systemd-service \
    udev-rules

SUMMARY="A tool to configure a CUPS server (often the local machine) using the CUPS API"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( linguas: ar as bg bn bn_IN br bs ca cs cy da de el en_GB es et fa fi fr gu he hi hr hu id is
               it ja kn ko lt lv mai ml mr ms nb nds nl nn or pa pl pt pt_BR ro ru si sk sl sr
               sr@latin sv ta te th tr uk vi zh_CN zh_TW )
"

RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2
        dev-util/desktop-file-utils
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        dev-libs/libsecret:1[gobject-introspection]
        dev-libs/libusb:1
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/pycups[python_abis:*(-)?]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        dev-python/requests[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
    suggestion:
        sys-auth/polkit [[
            description = [ ${PN} can use polkit for authentication ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/2766d74a98e44693f5e966d541a6303b966c088b.patch
    "${FILES}"/e7940de50a7164a2de82e003222ecbf8b10c9ad5.patch
    "${FILES}"/42d95b7560c26f0fdf3b4f46823843861948c136.patch
)

# FIXME: don't install .py files into /usr/share/${PN}
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --with-desktop-vendor="Exherbo"
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
    --with-udev-rules
    --with-udevdir=${UDEVDIR}
)

src_install() {
    default

    edo sed \
        -e "s|#!.*|#!/usr/host/bin/python$(python_get_abi)|" \
        -i "${IMAGE}"/usr/share/system-config-printer/system-config-printer.py
}

